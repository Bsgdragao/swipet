package com.brunoguerra.regioest1;



import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private LinearLayout layouts;
    private TextView tvRegioes;
    private TextView tvEstados;
    private ImageView ivSetBandeira;

    private LinearLayout layout;
    private int contR = 0;
    private int contE = 1;


    public String[][] regioes = new String[][]{{"Centro Oeste", "Brasília", "Góias", "Mato Grosso", "Mato Grosso do Sul"},
            {"Nordeste", "Alagoas", "Bahia", "Ceará", "Maranhão", "Paraíba", "Pernambuco", "Piauí", "Rio Grande do Norte", "Sergipe"},
            {"Norte", "Acre", "Amapá", "Amazonas", "Pará", "Rondônia", "Roraima", "Tocantins"},
            {"Sudeste", "Espirito Santo", "Minas Gerais", "Rio de Janeiro", "São Paulo"},
            {"Sul", "Paraná", "Rio Grande do Sul", "Santa Catarina"}};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regioes);

        layout = (LinearLayout) findViewById(R.id.layout);
        tvRegioes = (TextView) findViewById(R.id.tvRegiao);
        tvEstados = (TextView) findViewById(R.id.tvEstado);
        ivSetBandeira = (ImageView) findViewById(R.id.ivBandeira);

        telaP();
        setBands();


        layout.setOnTouchListener(new OnSwipeTouchListener(this) {


            @Override
            public void onSwipeRight() {
                super.onSwipeRight();


                if (contE > 1 ) {

                    contE--;
                }

                telaP();
                setBands();


            }

            @Override
            public void onSwipeLeft() {
                super.onSwipeLeft();
                if (contE <= regioes[contR][contE].length() || contE<9) {

                    contE++;
                }
                telaP();
                setBands();
            }


            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                contE=1;
                if (contR > 0) {

                    contR--;
                }
                telaP();
                setBands();


            }

            @Override
            public void onSwipeBottom() {
                super.onSwipeBottom();
                contE=1;
                    if (contR < 5) {

                        contR++;
                    }
                    telaP();
                    setBands();
                }


        });


    }

    public void telaP() {

        tvRegioes.setText(regioes[contR][0]);
        tvEstados.setText(regioes[contR][contE]);




    }

    public void setBands(){

        String nome = tvEstados.getText().toString();


        switch (nome) {

            case "Brasília":

                ivSetBandeira.setImageResource(R.drawable.brasilia2);
                break;

            case "Góias":
                ivSetBandeira.setImageResource(R.drawable.goias2);

                break;

            case "Mato Grosso":
                ivSetBandeira.setImageResource(R.mipmap.matogrosso2);
                break;

            case "Mato Grosso do Sul":
                ivSetBandeira.setImageResource(R.drawable.matogrossodosul2);

                break;

            case "Alagoas":
                ivSetBandeira.setImageResource(R.drawable.alagoas2);

                break;

            case "Bahia":
                ivSetBandeira.setImageResource(R.drawable.bahia2);

                break;

            case "Ceará":
                ivSetBandeira.setImageResource(R.drawable.ceara2);

                break;

            case "Maranhão":
                ivSetBandeira.setImageResource(R.drawable.maranhao2);

                break;

            case "Paraíba":
                ivSetBandeira.setImageResource(R.drawable.paraiba2);

                break;

            case "Pernambuco":
                ivSetBandeira.setImageResource(R.drawable.pernambuco2);

                break;

            case "Piauí":
                ivSetBandeira.setImageResource(R.drawable.piaui2);

                break;

            case "Rio Grande do Norte":
                ivSetBandeira.setImageResource(R.drawable.riograndedonorte2);

                break;

            case "Sergipe":
                ivSetBandeira.setImageResource(R.drawable.sergipe2);

                break;

            case "Acre":
                ivSetBandeira.setImageResource(R.drawable.acre2);

                break;

            case "Amapá":
                ivSetBandeira.setImageResource(R.drawable.amapa2);

                break;

            case "Amazonas":
                ivSetBandeira.setImageResource(R.drawable.amazonas2);

                break;

            case "Pará":
                ivSetBandeira.setImageResource(R.drawable.para2);

                break;

            case "Rondônia":
                ivSetBandeira.setImageResource(R.drawable.rondonia2);

                break;

            case "Roraima":
                ivSetBandeira.setImageResource(R.drawable.roraima2);

                break;

            case "Tocantins":
                ivSetBandeira.setImageResource(R.drawable.tocantins2);

                break;

            case "Espirito Santo":
                ivSetBandeira.setImageResource(R.drawable.espiritosanto2);

                break;

            case "Minas Gerais":
                ivSetBandeira.setImageResource(R.drawable.minasgerais2);

                break;

            case "Rio de Janeiro":
                ivSetBandeira.setImageResource(R.drawable.riodejaneiro2);

                break;

            case "São Paulo":
                ivSetBandeira.setImageResource(R.drawable.saopaulo2);

                break;

            case "Paraná":
                ivSetBandeira.setImageResource(R.drawable.parana2);

                break;

            case "Rio Grande do Sul":
                ivSetBandeira.setImageResource(R.drawable.riograndedosul2);

                break;

            case "Santa Catarina":
                ivSetBandeira.setImageResource(R.drawable.santacatarina2);

                break;

        }

    }


}
